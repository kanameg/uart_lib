/* Name: main.c
 * Author: <insert your name here>
 * Copyright: <insert your copyright message here>
 * License: <insert your license reference here>
 */

#include <avr/io.h>
#include "uart_lib.h"

int main(void)
{
    char line[32] = {0};
    
    /* insert your hardware initialization here */
    uart_init(0x33);
    sei();

    for(;;){
        /* insert your main loop code here */
        uart_gets(line);
        uart_puts(line); uart_puts("\n");
    }
    return 0;   /* never reached */
}
