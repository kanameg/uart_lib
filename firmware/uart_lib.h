/*! 
  @file       uart_lib.h
  @brief      ATTiny2313用 UARTライブラリ
  @author     kaname.g@gmail.com
  @note
*/

#ifndef UART_LIB_H
#define UART_LIB_H

#include <avr/io.h>
#include <avr/interrupt.h>
#include <inttypes.h>

/*!
  @breif      API 一覧
 */
void    uart_init(uint16_t baud);
void    uart_puts(const char *s);
uint8_t uart_gets(char *s);


#endif /* UART_LIB_H */
